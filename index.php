<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
session_start();

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

require_once "includes.php";

$factory = Factory::getInstance();
Config::init();
$log = new Login();



//Config::init();
//$log = new Login();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

//DEBUGGING
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


function printNicely($value){
    print nl2br(print_r($value, true));
}

function sortByOrder($a, $b) {
    return $a['total'] - $b['total'];
}



//SET CONDITIONS
\Slim\Route::setDefaultConditions(array(
    'class' => '[a-zA-Z]{3,}',
    'schoolId' =>'[0-9]{0,10000}',
    'competitionId' =>'[0-9]{0,10000}',
    'testId' =>'[0-9]{0,10000}',
    'id' =>'[0-9]{0,10000}',
    'schoolCompetitionId' =>'[0-9]{0,10000}',
    'school_id' =>'[0-9]{0,10000}',
));

$settings = array(
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'database' => 'educontest_test',
    'username' => 'root',
    'password' => 'root',
    'collation' => 'utf8_general_ci',
    'prefix' => ''
);

//$settings = array(
//    'driver' => 'mysql',
//    'host' => '127.0.0.1',
//    'database' => 'edudavid_edu',
//    'username' => 'edudavid_davey',
//    'password' => '!Rishman1',
//    'collation' => 'utf8_general_ci',
//    'prefix' => ''
//);



$container = new Illuminate\Container\Container;
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory($container);
$conn = $connFactory->make($settings);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);

require_once "DatabaseExceptionHandler.php";

$container->singleton(
    'Illuminate\Contracts\Debug\ExceptionHandler'
    , 'My\Custom\DatabaseExceptionHandler'
);





//BOOTSTRAP VIEWS

//public pages
require_once "app/views/pages.php";
//registration and authentication views
require_once "app/views/registration-authentication.php";
//admin
require_once "app/views/admin.php";
//user
require_once "app/views/users.php";


//BOOTSTRAP API

//iRestService
require_once "app/api/iRest.php";


function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


$app->get(
    '/lumen',
    function () use ($app){

        try{
            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
                ->setUsername(Config::EMAIL_USER)
                ->setPassword(Config::EMAIL_PASSWORD);
            $mailer = Swift_Mailer::newInstance($transport);

            $message = Swift_Message::newInstance("Welcome " . "Davey")
                ->setFrom(array(Config::EMAIL_USER => 'Educontest'))
                ->setTo(array('davidmcnight@gmail.com'))
                ->setBody('Hello Davey', "text/html");
            $result = $mailer->send($message);



    }catch (Exception $e){
            echo $e->getMessage();
        }
    }
);





/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
